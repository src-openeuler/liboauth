Name:           liboauth
Version:        1.0.3
Release:        15
Summary:        OAuth library functions
License:        MIT
URL:            http://liboauth.sourceforge.net/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz

Patch1:    backport-pedantic-ANSI-ISO-C-89-90-for-MSVC-2013-fixes-2.patch
Patch2:    backport-NOOP-whitespace-indent.patch
Patch3:    backport-Update-OpenSSL-1.1.0-API.patch

BuildRequires:  gcc curl-devel openssl-devel
# fix the failure of the test_uniencoding testcase
BuildRequires:  glibc-locale-archive

%description
a POSIX-C implementation of the http://oauth.net/ protocol. libOauth provides
functionality to encode URLs and sign HTTP request data according to the oAuth standard.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
Libraries and header files for developing applications.

%package	help
Summary:	help package for %{name} with man docs

%description	help
document files for %{name}

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
make


%install
%make_install
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%ldconfig_scriptlets

%check
make check

%files
%doc AUTHORS COPYING.MIT README
%{_libdir}/*.so.*

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/oauth.pc

%files help
%{_mandir}/man3/oauth.*
%doc ChangeLog

%changelog
* Fri Jul 19 2024 yixiangzhike <yixiangzhike007@163.com> - 1.0.3-15
- Use openssl instead of nss to fix failure for RSA-SHA1 signature

* Mon Jun 13 2022 yixiangzhike <yixiangzhike007@163.com> - 1.0.3-14
- Fix the failure of the test_uniencoding testcase

* Fri Feb 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0.3-13
- Make check after installation

* Fri Sep 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.3-12
- Package init

